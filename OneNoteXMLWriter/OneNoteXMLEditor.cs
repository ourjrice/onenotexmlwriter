﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OneNote = Microsoft.Office.Interop.OneNote;
using System.Linq;

namespace OneNoteXMLWriter
{
    public partial class FormOneNoteXMLEditor : Form
    {
        OneNote.Application OneNoteApp = new OneNote.Application();

        public FormOneNoteXMLEditor()
        {
            InitializeComponent();
        }

        private void FormOneNoteXMLEditor_Load(object sender, EventArgs e)
        {
            formtitle = this.Text;
            textBoxONXML.AcceptsTab = true;
            loadOneNoteXML();
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            loadOneNoteXML();
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                writeOneNoteXML(textBoxONXML.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FormOneNoteXMLEditor_Enter(object sender, EventArgs e)
        {
            //loadOneNoteXML();
        }

		private void labelColorPicker_Click(object sender, EventArgs e)
		{
			colorDialog1.Color = textBoxONXML.ForeColor;
			if (colorDialog1.ShowDialog() == DialogResult.OK)
			{
				textBoxONXML.ForeColor = colorDialog1.Color;
				labelColorPicker.BackColor = colorDialog1.Color;
			}
		}

		private void labelBackgroundColorPicker_Click(object sender, EventArgs e)
		{
			colorDialog1.Color = textBoxONXML.BackColor;
			if (colorDialog1.ShowDialog() == DialogResult.OK)
			{
				textBoxONXML.BackColor = colorDialog1.Color;
				labelBackgroundColorPicker.BackColor = colorDialog1.Color;
			}

		}

		private void checkBoxWordWrap_CheckedChanged(object sender, EventArgs e)
		{
			if (checkBoxWordWrap.Checked)
				textBoxONXML.WordWrap = true;
			else
				textBoxONXML.WordWrap = false;
		}

		private string getOneNoteXML()
        {
            OneNote.Window current = OneNoteApp.Windows.CurrentWindow;
            string pageXML = "";
            if (current != null)
                OneNoteApp.GetHierarchy(current.CurrentPageId, OneNote.HierarchyScope.hsChildren, out pageXML);
            return pageXML;
        }

        private void writeOneNoteXML(string xml)
        {
            OneNoteApp.UpdatePageContent(xml);
            OneNoteApp.SyncHierarchy(OneNoteApp.Windows.CurrentWindow.CurrentNotebookId);
            loadOneNoteXML();
        }

        string formtitle;

        private void loadOneNoteXML()
        {
            
            string xml = getOneNoteXML();
            XDocument xdoc = XDocument.Parse(xml);
            XNamespace URI = xdoc.Root.GetNamespaceOfPrefix("one");
            this.Text = formtitle + " | " + XDocument.Parse(xml).Descendants(URI + "Title").Descendants(URI + "T").First().Value;
            if (xml.Length > 0)
                textBoxONXML.Text = xdoc.ToString();
        }

		private void textBoxSearch_TextChanged(object sender, EventArgs e)
		{

		}

		int searchindex;
        int linecount;
        int foundindex;
        int currentindex;

		private void buttonSearchNext_Click(object sender, EventArgs e)
		{

			if (searchindex <= 0)
				searchindex = textBoxONXML.Find(textBoxSearch.Text, RichTextBoxFinds.MatchCase);
			else
				searchindex = textBoxONXML.Find(textBoxSearch.Text, searchindex + 1, RichTextBoxFinds.MatchCase);

		}

		private void textBoxSearch_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)Keys.Enter)
				buttonSearchNext_Click(sender, e);
		}

        private void buttonInnerText_Click(object sender, EventArgs e)
        {
            XDocument xdoc = XDocument.Parse(textBoxONXML.Text);
            XNamespace URI = xdoc.Root.GetNamespaceOfPrefix("one");
            string innertext = "";

            if (textBoxSearch.Text.Length > 0 & !textBoxSearch.Text.Contains(xdoc.Root.GetPrefixOfNamespace(URI)))
                foreach (XElement el in xdoc.Root.Descendants(URI + textBoxSearch.Text))
                {
                    if (el.Value.Length > 0)
                        innertext += el.Value + Environment.NewLine; ;
                }

            if (innertext.Length > 0)
            {
                Clipboard.SetText(innertext);
                MessageBox.Show(string.Format("All {0}:{1} values have been copied to the Clipboard", xdoc.Root.GetPrefixOfNamespace(URI), textBoxSearch.Text));
            }
               

        }
    }
}
