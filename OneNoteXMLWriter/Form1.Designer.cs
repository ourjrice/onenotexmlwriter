﻿namespace OneNoteXMLWriter
{
    partial class FormOneNoteXMLEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelControlStrip = new System.Windows.Forms.Panel();
            this.buttonSearchNext = new System.Windows.Forms.Button();
            this.labelSearch = new System.Windows.Forms.Label();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.checkBoxWordWrap = new System.Windows.Forms.CheckBox();
            this.labelBackground = new System.Windows.Forms.Label();
            this.labelBackgroundColorPicker = new System.Windows.Forms.Label();
            this.labelFontColor = new System.Windows.Forms.Label();
            this.labelColorPicker = new System.Windows.Forms.Label();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.textBoxONXML = new System.Windows.Forms.RichTextBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.buttonInnerText = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelControlStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panelControlStrip, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxONXML, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(837, 598);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panelControlStrip
            // 
            this.panelControlStrip.Controls.Add(this.buttonInnerText);
            this.panelControlStrip.Controls.Add(this.buttonSearchNext);
            this.panelControlStrip.Controls.Add(this.labelSearch);
            this.panelControlStrip.Controls.Add(this.textBoxSearch);
            this.panelControlStrip.Controls.Add(this.checkBoxWordWrap);
            this.panelControlStrip.Controls.Add(this.labelBackground);
            this.panelControlStrip.Controls.Add(this.labelBackgroundColorPicker);
            this.panelControlStrip.Controls.Add(this.labelFontColor);
            this.panelControlStrip.Controls.Add(this.labelColorPicker);
            this.panelControlStrip.Controls.Add(this.buttonReset);
            this.panelControlStrip.Controls.Add(this.buttonSubmit);
            this.panelControlStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlStrip.Location = new System.Drawing.Point(3, 559);
            this.panelControlStrip.Name = "panelControlStrip";
            this.panelControlStrip.Size = new System.Drawing.Size(831, 36);
            this.panelControlStrip.TabIndex = 2;
            // 
            // buttonSearchNext
            // 
            this.buttonSearchNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSearchNext.Location = new System.Drawing.Point(436, 8);
            this.buttonSearchNext.Name = "buttonSearchNext";
            this.buttonSearchNext.Size = new System.Drawing.Size(29, 21);
            this.buttonSearchNext.TabIndex = 10;
            this.buttonSearchNext.Text = ">";
            this.buttonSearchNext.UseVisualStyleBackColor = true;
            this.buttonSearchNext.Click += new System.EventHandler(this.buttonSearchNext_Click);
            // 
            // labelSearch
            // 
            this.labelSearch.AutoSize = true;
            this.labelSearch.Location = new System.Drawing.Point(104, 12);
            this.labelSearch.Name = "labelSearch";
            this.labelSearch.Size = new System.Drawing.Size(44, 13);
            this.labelSearch.TabIndex = 8;
            this.labelSearch.Text = "Search:";
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Location = new System.Drawing.Point(154, 9);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(276, 20);
            this.textBoxSearch.TabIndex = 7;
            this.textBoxSearch.TextChanged += new System.EventHandler(this.textBoxSearch_TextChanged);
            this.textBoxSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSearch_KeyPress);
            // 
            // checkBoxWordWrap
            // 
            this.checkBoxWordWrap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWordWrap.AutoSize = true;
            this.checkBoxWordWrap.Checked = true;
            this.checkBoxWordWrap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxWordWrap.Location = new System.Drawing.Point(585, 14);
            this.checkBoxWordWrap.Name = "checkBoxWordWrap";
            this.checkBoxWordWrap.Size = new System.Drawing.Size(81, 17);
            this.checkBoxWordWrap.TabIndex = 6;
            this.checkBoxWordWrap.Text = "Word Wrap";
            this.checkBoxWordWrap.UseVisualStyleBackColor = true;
            this.checkBoxWordWrap.CheckedChanged += new System.EventHandler(this.checkBoxWordWrap_CheckedChanged);
            // 
            // labelBackground
            // 
            this.labelBackground.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelBackground.AutoSize = true;
            this.labelBackground.Location = new System.Drawing.Point(4, 20);
            this.labelBackground.Name = "labelBackground";
            this.labelBackground.Size = new System.Drawing.Size(59, 13);
            this.labelBackground.TabIndex = 5;
            this.labelBackground.Text = "Back Color";
            // 
            // labelBackgroundColorPicker
            // 
            this.labelBackgroundColorPicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelBackgroundColorPicker.BackColor = System.Drawing.Color.White;
            this.labelBackgroundColorPicker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBackgroundColorPicker.Location = new System.Drawing.Point(69, 22);
            this.labelBackgroundColorPicker.Name = "labelBackgroundColorPicker";
            this.labelBackgroundColorPicker.Size = new System.Drawing.Size(12, 12);
            this.labelBackgroundColorPicker.TabIndex = 4;
            this.labelBackgroundColorPicker.Click += new System.EventHandler(this.labelBackgroundColorPicker_Click);
            // 
            // labelFontColor
            // 
            this.labelFontColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelFontColor.AutoSize = true;
            this.labelFontColor.Location = new System.Drawing.Point(8, 5);
            this.labelFontColor.Name = "labelFontColor";
            this.labelFontColor.Size = new System.Drawing.Size(55, 13);
            this.labelFontColor.TabIndex = 3;
            this.labelFontColor.Text = "Font Color";
            // 
            // labelColorPicker
            // 
            this.labelColorPicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelColorPicker.BackColor = System.Drawing.Color.Black;
            this.labelColorPicker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelColorPicker.Location = new System.Drawing.Point(69, 6);
            this.labelColorPicker.Name = "labelColorPicker";
            this.labelColorPicker.Size = new System.Drawing.Size(12, 12);
            this.labelColorPicker.TabIndex = 2;
            this.labelColorPicker.Click += new System.EventHandler(this.labelColorPicker_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReset.Location = new System.Drawing.Point(672, 7);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 1;
            this.buttonReset.Text = "Load";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSubmit.Location = new System.Drawing.Point(753, 7);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(75, 23);
            this.buttonSubmit.TabIndex = 0;
            this.buttonSubmit.Text = "Submit";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // textBoxONXML
            // 
            this.textBoxONXML.AcceptsTab = true;
            this.textBoxONXML.AutoWordSelection = true;
            this.textBoxONXML.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxONXML.Font = new System.Drawing.Font("Courier New", 8F);
            this.textBoxONXML.HideSelection = false;
            this.textBoxONXML.Location = new System.Drawing.Point(3, 3);
            this.textBoxONXML.Name = "textBoxONXML";
            this.textBoxONXML.Size = new System.Drawing.Size(831, 550);
            this.textBoxONXML.TabIndex = 3;
            this.textBoxONXML.Text = "";
            // 
            // buttonInnerText
            // 
            this.buttonInnerText.Location = new System.Drawing.Point(471, 9);
            this.buttonInnerText.Name = "buttonInnerText";
            this.buttonInnerText.Size = new System.Drawing.Size(75, 20);
            this.buttonInnerText.TabIndex = 12;
            this.buttonInnerText.Text = "Inner Text";
            this.buttonInnerText.UseVisualStyleBackColor = true;
            this.buttonInnerText.Click += new System.EventHandler(this.buttonInnerText_Click);
            // 
            // FormOneNoteXMLEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 598);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(853, 637);
            this.Name = "FormOneNoteXMLEditor";
            this.Text = "OneNote XML Editor";
            this.Load += new System.EventHandler(this.FormOneNoteXMLEditor_Load);
            this.Enter += new System.EventHandler(this.FormOneNoteXMLEditor_Enter);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panelControlStrip.ResumeLayout(false);
            this.panelControlStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panelControlStrip;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonSubmit;
		private System.Windows.Forms.RichTextBox textBoxONXML;
		private System.Windows.Forms.Label labelColorPicker;
		private System.Windows.Forms.ColorDialog colorDialog1;
		private System.Windows.Forms.Label labelBackground;
		private System.Windows.Forms.Label labelBackgroundColorPicker;
		private System.Windows.Forms.Label labelFontColor;
		private System.Windows.Forms.CheckBox checkBoxWordWrap;
		private System.Windows.Forms.Label labelSearch;
		private System.Windows.Forms.TextBox textBoxSearch;
		private System.Windows.Forms.Button buttonSearchNext;
        private System.Windows.Forms.Button buttonInnerText;
    }
}

